/**
 * 
 */
package todo.model;

import java.sql.Date;
import java.sql.Timestamp;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author allen
 *
 */
@XmlRootElement
public class Todo {
	private int id;
	private String name;
	private String owner;
	private String priority;
	private String classifier;
	public String getClassifier() {
		return classifier;
	}
	public void setClassifier(String classifier) {
		this.classifier = classifier;
	}
	public double getAccuracyclassifier() {
		return accuracyclassifier;
	}
	public void setAccuracyclassifier(double accuracyclassifier) {
		this.accuracyclassifier = accuracyclassifier;
	}
	public Timestamp getDateoftest() {
		return dateoftest;
	}
	public void setDateoftest(Timestamp dateoftest) {
		this.dateoftest = dateoftest;
	}
	public String getFacialalgorithm() {
		return facialalgorithm;
	}
	public void setFacialalgorithm(String facialalgorithm) {
		this.facialalgorithm = facialalgorithm;
	}
	public int getNooffaces() {
		return nooffaces;
	}
	public void setNooffaces(int nooffaces) {
		this.nooffaces = nooffaces;
	}
	public String getDatabasename() {
		return databasename;
	}
	public void setDatabasename(String databasename) {
		this.databasename = databasename;
	}
	private double accuracyclassifier;
	private Timestamp dateoftest;
	private String facialalgorithm;
	private int nooffaces;
	private String databasename;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getOwner() {
		return owner;
	}
	public void setOwner(String owner) {
		this.owner = owner;
	}
	public String getPriority() {
		return priority;
	}
	public void setPriority(String priority) {
		this.priority = priority;
	}
}
