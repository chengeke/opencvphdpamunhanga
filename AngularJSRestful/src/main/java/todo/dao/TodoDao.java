/**
 * 
 */
package todo.dao;

import java.util.ArrayList;
import java.util.List;

import dao.AccessManager;
import todo.model.Todo;

/**
 * @author allen
 *
 */
public class TodoDao {
	private static int idSeq = 1;
	private static List<Todo> todos;
	
	static{
		final Todo todo = new Todo(){{
			setId(idSeq++);
			setName("CVTest1-LBP");
			setOwner("Allen");
			setPriority("High");
		}};
		try {
			todos = new AccessManager().getCourses();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
				//new ArrayList<Todo>(){{
			//add(todo);
		//}};
	}
	
	public static List<Todo> queryAll(){
		
		System.out.println("skskskskserr qurry all");
		try{
		todos=new AccessManager().getCourses();
		}catch(Exception e)
		{}
		return todos;
	}
	
	public static Todo getTodo(int id){
		System.out.println("aaaanm");
		Todo result = null;
		for(Todo todo: todos){
			if(todo.getId() == Integer.valueOf(id)){
				result = todo;
				break;
			}
		}
		return result;
	}
	
	public static Todo addTodo(Todo todo){
		todo.setId(idSeq++);
		todos.add(todo);
		try{
		AccessManager.addCourse(todo);
		System.out.println("aaaaklio"+todo.toString());
		}catch(Exception e){}
		System.out.println("aaaakl");
		return todo;
	}

	public static void updateTodo(Todo todo) {
		Todo target = getTodo(todo.getId());
		target.setName(todo.getName());
		target.setOwner(todo.getOwner());
		target.setPriority(todo.getPriority());
		try{
			AccessManager.updateCourse(todo);
			}catch(Exception e){}
	}

	public static void deleteTodo(Integer id) {
		System.out.println("aannaa");
		int index = 0;
		for(int i=0;i<todos.size();i++){
			if(todos.get(i).getId() == id) index = i;
		}
		try{
		
		AccessManager.deleteCourse(id);
		}catch(Exception e){}
		todos.remove(index);
	}
	
}
