package dao;
 
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Random;

import dto.Experiment;
import todo.model.Todo;
 
public class Access
{
public ArrayList<Todo> getCourses(Connection con) throws SQLException
{
ArrayList<Todo> courseList = new ArrayList<Todo>();
PreparedStatement stmt = con.prepareStatement("SELECT * FROM Experiment");
ResultSet rs = stmt.executeQuery();
try
{
while(rs.next())
{
	Todo courseObj = new Todo();
courseObj.setId(rs.getInt("id"));
courseObj.setName(rs.getString("name"));
courseObj.setOwner(rs.getString("owner"));
courseObj.setPriority(rs.getString("priorty"));
courseObj.setClassifier(rs.getString("classifier"));
courseObj.setAccuracyclassifier(rs.getDouble("accuracyclassifier"));
courseObj.setDateoftest(rs.getTimestamp("dateoftest"));
courseObj.setFacialalgorithm(rs.getString("facialalgorithm"));
courseObj.setNooffaces(rs.getInt("nooffaces"));
courseObj.setDatabasename(rs.getString("databasename"));
System.out.println(courseObj.getOwner()+"sss");
courseList.add(courseObj);
}
} catch (SQLException e)
{
e.printStackTrace();
}
return courseList;
 
}

public static int addCourse(Todo course,Connection con) throws SQLException
{int id4=0;
try
{


// the mysql insert statement
String query = " insert into Experiment (id, name, owner, priorty,classifier,"
		+ "accuracyclassifier,facialalgorithm,nooffaces,databasename,dateoftest)"
        + " values (?, ?, ?, ?,?,?,?,?,?,?)";

// create the mysql insert preparedstatement
PreparedStatement preparedStmt = con.prepareStatement(query);
id4=(int)Math.round(new Random().nextInt(100)+Math.random()+9*Math.random());
preparedStmt.setInt (1, id4);
preparedStmt.setString (2, course.getName());
preparedStmt.setString(3, course.getOwner());
preparedStmt.setString(4, course.getPriority());
preparedStmt.setString (5, course.getClassifier());
preparedStmt.setDouble(6, course.getAccuracyclassifier());
java.sql.Timestamp date2 = new java.sql.Timestamp(new java.util.Date().getTime());
preparedStmt.setTimestamp(10, date2);
preparedStmt.setString(7, course.getFacialalgorithm());
preparedStmt.setInt (8, course.getNooffaces());
preparedStmt.setString(9, course.getDatabasename());

// execute the preparedstatement
preparedStmt.execute();


} catch (SQLException e)
{
e.printStackTrace();
}

 return id4;
}
public static void updateCourse(Todo course,Connection con) throws SQLException
{
try
{


// the mysql insert statement
String query = " update  yourdatabasename.Experiment  SET "
		+ " name=?, "
		+ "owner=?, "
		+ "priorty=?,"
		+ "classifier=?,"
		+ "accuracyclassifier=?,"
		+ "facialalgorithm=?,"
		+ "nooffaces=?,"
		+ "dateoftest=?,"
		+ "databasename=?"
        + " where id=?";

// create the mysql insert preparedstatement
PreparedStatement preparedStmt = con.prepareStatement(query);

preparedStmt.setString (1, course.getName());
preparedStmt.setString(2, course.getOwner());
preparedStmt.setString(3, course.getPriority());
preparedStmt.setString (4, course.getClassifier());
preparedStmt.setDouble(5, course.getAccuracyclassifier());
java.sql.Timestamp date2 = new java.sql.Timestamp(new java.util.Date().getTime());
preparedStmt.setTimestamp(8, date2);
preparedStmt.setString(6, course.getFacialalgorithm());
preparedStmt.setInt (7, course.getNooffaces());
preparedStmt.setString(9, course.getDatabasename());
preparedStmt.setInt (10, course.getId());
// execute the preparedstatement
System.out.println(preparedStmt.toString());
preparedStmt.execute();


} catch (SQLException e)
{
e.printStackTrace();
}

 
}

public static void deleteCourse(int id,Connection con) throws SQLException
{
try
{


// the mysql insert statement
String query = " delete from Experiment where id=?";

// create the mysql insert preparedstatement
PreparedStatement preparedStmt = con.prepareStatement(query);
preparedStmt.setInt (1,id);

// execute the preparedstatement
preparedStmt.execute();


} catch (SQLException e)
{
e.printStackTrace();
}

 
}
}