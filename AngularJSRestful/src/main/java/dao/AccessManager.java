package dao;
 
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
 
import dao.Access;
import dao.Database;
import dto.Experiment;
import todo.model.Todo;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
public class AccessManager
{
public ArrayList<Todo> getCourses() throws Exception
{
ArrayList<Todo> courseList = new ArrayList<Todo>();
Database db = new Database();
Connection con = db.getConnection();
Access access = new Access();
courseList = access.getCourses(con);
System.out.println("Output from  .... \n");
return courseList;
}
public static void addCourse(Todo course) throws Exception
{

Database db = new Database();
Connection con = db.getConnection();
int id4=Access.addCourse(course, con);
course.setId(id4);
callRest(course);

}

public static void callRest(Todo course)
{
	try {
		System.out.println("Output from Server .... \n");
		int k=course.getNooffaces();
		int kk=course.getId();
		String databasename=course.getDatabasename();
		String classifier=course.getClassifier();
		String facealgorithm=course.getFacialalgorithm();
		facealgorithm=facealgorithm.replaceAll(" ", "+");
		databasename=databasename.replaceAll(" ", "+");
		classifier=classifier.replaceAll(" ", "+");
		String userInput="counter3="+k+"&id1="+kk+"&databasename="+databasename
			+"&classifier="+classifier+"&facealgorithm="+facealgorithm;
		System.out.println("Output from Server .... \n"+userInput);
		URL url = new URL("http://127.0.0.1:5000/hello?"+userInput);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("GET");
		conn.setRequestProperty("Accept", "application/json");

		if (conn.getResponseCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ conn.getResponseCode());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader(
			(conn.getInputStream())));

		String output;
		System.out.println("Output from Server .... \n");
		while ((output = br.readLine()) != null) {
			System.out.println(output);
		}

		conn.disconnect();

	} catch (MalformedURLException e) {

		e.printStackTrace();

	} catch (IOException e) {

		e.printStackTrace();

	}
	



}
public static void deleteCourse(int id) throws Exception
{

Database db = new Database();
Connection con = db.getConnection();
Access.deleteCourse(id, con);

}

public static void updateCourse(Todo course) throws Exception
{

Database db = new Database();
Connection con = db.getConnection();
Access.updateCourse(course, con);

}

public static void main(String [] Args)
{
	
System.out.println("sss");
callRest(null);
ArrayList<Todo> ddd;
try {
	ddd = new AccessManager().getCourses();
	System.out.println(ddd.get(0).getName());
} catch (Exception e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}


}




}