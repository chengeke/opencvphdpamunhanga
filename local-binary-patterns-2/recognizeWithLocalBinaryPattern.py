# USAGE
# python recognize.py --training images/training --testing images/testing

# import the necessary packages
from pyimagesearch.localbinarypatterns import LocalBinaryPatterns
from pyimagesearch import  PlotLocalBinaryPatterns
from skimage.feature import local_binary_pattern
import matplotlib
from skimage import data as data9
# Load libraries
from scipy.stats import itemfreq
from sklearn.preprocessing import normalize
import pandas
from pandas.tools.plotting import scatter_matrix
import matplotlib.pyplot as plt
from sklearn import model_selection
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
import matplotlib.pyplot as plt
import numpy as np
import scipy.ndimage as nd
import skimage.feature as ft
import pandas
from pandas.tools.plotting import scatter_matrix
import matplotlib.pyplot as plt
from sklearn import model_selection
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC

from sklearn.svm import LinearSVC
from imutils import paths
import argparse
import cv2
from  sklearn  import svm
from timeit import default_timer as timer
import scipy  as scipy1
import numpy as np
import matplotlib.pylab as plt
import warnings
warnings.filterwarnings("ignore")
from sklearn import datasets
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import style
# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
#ap.add_argument("-t", "--training", required=True,
	#help="path to the training images")
#ap.add_argument("-e", "--testing", required=True, 
	#help="path to the tesitng images")
args = vars(ap.parse_args())

# initialize the local binary patterns descriptor along with
# the data and label lists
desc = LocalBinaryPatterns(24, 8)
data = []
labels = []
import numpy as np
import matplotlib.pyplot as plt

from astropy.visualization import (MinMaxInterval, SqrtStretch,
                                   ImageNormalize)
count=0
# loop over the training images
for imagePath in paths.list_images('images/training/cohn-kanade-images'):
	# load the image, convert it to grayscale, and describe it
	count=count+1
	if(count>12000):
		break
	image = cv2.imread(imagePath)
	# Create an ImageNormalize object
	#norm = ImageNormalize(image, interval=MinMaxInterval(),
    #                  stretch=SqrtStretch())
	#im = ax.imshow(image, origin='lower', norm=norm)
	gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
	hist = desc.describe(gray)
	
	# Calculate the histogram
	#x = itemfreq(hist2.ravel())
	# Normalize the histogram
	#hist9= x[:, 1]/sum(x[:, 1])
    # Display the query image
    #print(hist)
	# extract the label from the image path, then update the
	# label and data lists
	labels.append(imagePath.split("/")[-2])
	data.append(hist)
	print(count,": count: ",imagePath,"ddddddddd")
	lbp_image=local_binary_pattern(gray,8,2,method='uniform') 
	histogram=scipy1.stats.itemfreq(lbp_image)
	#print(histogram)
	
	count=count+1


	
	

# train a Linear SVM on the data
svmmodel = LinearSVC(C=100.0, random_state=42)
svmmodel.fit(data, labels)
modelsvm2=svm.SVC(kernel='rbf')
modelsvm2.fit(data, labels)	
from sklearn.ensemble import RandomForestClassifier
forestmodel  = RandomForestClassifier(n_estimators=10)
forestmodel.fit(data,labels)# = clf.fit(X, Y)
from sklearn.model_selection import cross_val_score
from sklearn.datasets import load_iris
from sklearn import metrics

from sklearn.ensemble import AdaBoostClassifier
adaboostmodel = AdaBoostClassifier(n_estimators=100)
adaboostmodel.fit(data, labels)
#KNN
# train a Linear SVM on the data
#model = LinearSVC(C=100.0, random_state=42)
from sklearn import neighbors
knnmodel = neighbors.KNeighborsClassifier(n_neighbors=3,algorithm='kd_tree')
knnmodel.fit(data, labels)
y_pred = knnmodel.predict(data)
print("kk11 KNN",metrics.accuracy_score(labels, y_pred))
# STEP 1: split X and y into training and testing sets
from sklearn.cross_validation import train_test_split


from sklearn.svm import SVC
svmmodel= LinearSVC(C=100.0, random_state=42)
svmmodel.fit(data, labels)
svm_pred = svmmodel.predict(data)
print("kk11 linear svm",metrics.accuracy_score(labels,svm_pred))


#Cross Validation Test Scenario
X_train, X_test, y_train, y_test = train_test_split(data, labels, test_size=0.4, random_state=4)
knn = SVC(kernel='rbf', probability=True)
knn.fit(X_train, y_train)
y_pred = knn.predict(X_test)
print("mmken4n rbf svm is here ",metrics.accuracy_score(y_test, y_pred))

from sklearn.model_selection import cross_val_score
from sklearn.datasets import make_blobs
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import SVC
rfmodel=  RandomForestClassifier(random_state=1)
rfmodel.fit(data, labels)
rf_pred = rfmodel.predict(data)
print("kk11 rf",metrics.accuracy_score(labels,rf_pred))

etcmodel=  RandomForestClassifier(random_state=1)
etcmodel.fit(data, labels)
etcmodel_pred = etcmodel.predict(data)
print("kk11 etcmodel",metrics.accuracy_score(labels,etcmodel_pred))


	
from sklearn import neighbors
knnmodel = neighbors.KNeighborsClassifier(n_neighbors=3,algorithm='kd_tree')
knnmodel.fit(data, labels)
y_pred = knnmodel.predict(data)
print("kk11 KNN",metrics.accuracy_score(labels, y_pred))
# STEP 1: split X and y into training and testing sets


from sklearn.ensemble import VotingClassifier
# Training classifiers
X=data
y=labels
clf1 = DecisionTreeClassifier(max_depth=4)
clf2 = KNeighborsClassifier(n_neighbors=7)
clf3 = SVC(kernel='rbf', probability=True)
eclf = VotingClassifier(estimators=[('dt', clf1), ('knn', clf2), ('svc', clf3)], voting='hard', weights=[2,5,2])
clf1 = clf1.fit(X,y)
clf2 = clf2.fit(X,y)
clf3 = clf3.fit(X,y)
eclf = eclf.fit(X,y)
eclf_pred = eclf.predict(data)
print("kk11 eclf",metrics.accuracy_score(labels,eclf_pred))



'''

from sklearn.linear_model import LogisticRegression
xmodel = LogisticRegression()
xmodel.fit(data, labels)
import cPickle
f = open("model.cpickle", "w")
f.write(cPickle.dumps(xmodel))
f.close()
x2model = cPickle.loads(open("model.cpickle").read())
count=0
# loop over the testing images
for imagePath in paths.list_images('images/testing'):
	# load the image, convert it to grayscale, describe it,
	# and classify it
	image = cv2.imread(imagePath)
	gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
	hist = desc.describe(gray)
	
	prediction = x2model.predict(hist)[0]
	print("my preiction",imagePath,"ddd", prediction)
	print("LR",x2model.score(data[count:], labels[count:]))
	
	prediction = adaboostmodel.predict(hist)[0]
	predictions = adaboostmodel.predict(hist)
	#print("my adaboostmodel preiction", prediction,"and all=",predictions)
	print("adaboostmodel",adaboostmodel.score(data[count:], labels[count:]))
	scores = cross_val_score(adaboostmodel, data, labels)
	print("scoress mean",scores.mean())
	
	prediction = forestmodel.predict(hist)[0]
	print("my forestmodel preiction", prediction)
	print("forestmodel ",forestmodel.score(data[count:], labels[count:]))
	
	
	prediction = svmmodel.predict(hist)[0]
	print("SVM",svmmodel.score(data[count:], labels[count:]))
	y_pred = svmmodel.predict(data)
	print("kk11 SVM",metrics.accuracy_score(labels, y_pred))

	prediction = modelsvm2.predict(hist)[0]
	print("SVM2 ",modelsvm2.score(data[count:], labels[count:]))
	
	prediction = knnmodel.predict(hist)[0]
	print("kNN ",knnmodel.score(data[count:], labels[count:]))
	print("mmmm  ",prediction)
	count=count+1
	print("------------")
	# Learn about API authentication here: https://plot.ly/python/getting-started
	# Find your api_key here: https://plot.ly/settings/api
	# settings for LBP
	import os

	from pyimagesearch.PlotLocalBinaryPatterns import kiko 
	kiko.kenn("alldata/googleset/"+os.path.split(imagePath)[-1])
	
	#print(model.predict_proba([[3]]))
	# display the image and the prediction
	#cv2.putText(image, prediction, (10, 30), cv2.FONT_HERSHEY_SIMPLEX,
	#	1.0, (0, 0, 255), 3)
	#cv2.imshow("Image", image)
	#cv2.waitKey(0)
	



import pandas
from sklearn import model_selection
from sklearn.linear_model import LogisticRegression
url = "https://archive.ics.uci.edu/ml/machine-learning-databases/pima-indians-diabetes/pima-indians-diabetes.data"
names = ['preg', 'plas', 'pres', 'skin', 'test', 'mass', 'pedi', 'age', 'class']
dataframe = pandas.read_csv(url, names=names)
array = dataframe.values
X = array[:,0:8]
Y = array[:,8]
seed = 7
kfold = model_selection.KFold(n_splits=10, random_state=seed)
model = LogisticRegression()
scoring = 'accuracy'
results = model_selection.cross_val_score(model, X, Y, cv=kfold, scoring=scoring)
print("Accuracy: %.3f (%.3f)") % (results.mean(), results.std())



# Cross Validation Classification ROC AUC
import pandas
from sklearn import model_selection
from sklearn.linear_model import LogisticRegression
url = "https://archive.ics.uci.edu/ml/machine-learning-databases/pima-indians-diabetes/pima-indians-diabetes.data"
names = ['preg', 'plas', 'pres', 'skin', 'test', 'mass', 'pedi', 'age', 'class']
dataframe = pandas.read_csv(url, names=names)
array = dataframe.values
X = array[:,0:8]
Y = array[:,8]
seed = 7
kfold = model_selection.KFold(n_splits=10, random_state=seed)
model = LogisticRegression()
scoring = 'roc_auc'
results = model_selection.cross_val_score(model, X, Y, cv=kfold, scoring=scoring)
print("AUC: %.3f (%.3f)") % (results.mean(), results.std())






# Spot Check Algorithms
# Split-out validation dataset
# Load dataset
url = "https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data"
names = ['sepal-length', 'sepal-width', 'petal-length', 'petal-width', 'class']
dataset = pandas.read_csv(url, names=names)
array = dataset.values
X = array[:,0:4]
Y = array[:,4]
validation_size = 0.20
seed = 7
X_train, X_validation, Y_train, Y_validation = model_selection.train_test_split(X, Y, test_size=validation_size, random_state=seed)
models = []
models.append(('LR', LogisticRegression()))
models.append(('LDA', LinearDiscriminantAnalysis()))
models.append(('KNN', KNeighborsClassifier()))
models.append(('CART', DecisionTreeClassifier()))
models.append(('NB', GaussianNB()))
models.append(('SVM', SVC()))
# evaluate each model in turn
results = []
names = []
for name, model in models:
	kfold = model_selection.KFold(n_splits=10, random_state=seed)
	cv_results = model_selection.cross_val_score(model, X_train, Y_train, cv=kfold, scoring=scoring)
	results.append(cv_results)
	names.append(name)
	msg = "%s: %f (%f)" % (name, cv_results.mean(), cv_results.std())
	print(msg)
	'''
