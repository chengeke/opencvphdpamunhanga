"""
===============================================
Local Binary Pattern for texture classification
===============================================

In this example, we will see how to classify textures based on LBP (Local
Binary Pattern). The histogram of the LBP result is a good measure to classify
textures. For simplicity the histogram distributions are then tested against
each other using the Kullback-Leibler-Divergence.
"""

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import scipy.ndimage as nd
import skimage.feature as ft
from skimage import data
class kiko:
    def __init__(self, numPoints, radius):
        # store the number of points and radius
        self.numPoints = numPoints
        self.radius = radius

    @classmethod
    def kenn(self,imm):
        # settings for LBPprint("d  ",imm,"jjjjjjjjj")
        METHOD = 'uniform'
        P = 16
        R = 2
        matplotlib.rcParams['font.size'] = 9
        
        brick = data.load(imm)
        
        #grass = data.load('grass.png')
        #wall = data.load('rough-wall.png')
        refs = {
            'brick': ft.local_binary_pattern(brick, P, R, METHOD)
            }
        fig, ((ax1, ax2, ax3), (ax4, ax5, ax6)) = plt.subplots(nrows=2, ncols=3,figsize=(9, 6))
        plt.gray()
        ax1.imshow(brick)
        ax1.axis('off')
        ax4.hist(refs['brick'].ravel(), normed=True, bins=P + 2, range=(0, P + 2))
        ax4.set_ylabel('Percentage')
        #ax2.imshow(grass)
        #ax2.axis('off')
        #ax5.hist(refs['grass'].ravel(), normed=True, bins=P + 2, range=(0, P + 2))
        #ax5.set_xlabel('Uniform LBP values')
        #ax3.imshow(wall)
        #ax3.axis('off')
        #ax6.hist(refs['wall'].ravel(), normed=True, bins=P + 2, range=(0, P + 2))
        plt.show()
        return 0;


   
            
            