# USAGE
# python recognize.py --training images/training --testing images/testing

# import the necessary packages
from pyimagesearch.localbinarypatterns import LocalBinaryPatterns
from pyimagesearch import  PlotLocalBinaryPatterns
from skimage.feature import local_binary_pattern
import matplotlib
from skimage import data as data9

import matplotlib.pyplot as plt
import numpy as np
import scipy.ndimage as nd
import skimage.feature as ft
import pandas
from pandas.tools.plotting import scatter_matrix
import matplotlib.pyplot as plt
from sklearn import model_selection
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC

from sklearn.svm import LinearSVC
from imutils import paths
import argparse
import cv2
from  sklearn  import svm
from timeit import default_timer as timer
import scipy  as scipy1
import numpy as np
import matplotlib.pylab as plt
import warnings
warnings.filterwarnings("ignore")
from sklearn import datasets
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import style
# construct the argument parse and parse the arguments
ap = argparse.ArgumentParser()
#ap.add_argument("-t", "--training", required=True,
	#help="path to the training images")
#ap.add_argument("-e", "--testing", required=True, 
	#help="path to the tesitng images")
args = vars(ap.parse_args())

# initialize the local binary patterns descriptor along with
# the data and label lists
desc = LocalBinaryPatterns(24, 8)
data = []
labels = []

# loop over the training images
for imagePath in paths.list_images('images/training'):
	# load the image, convert it to grayscale, and describe it
	image = cv2.imread(imagePath)
	gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
	hist = desc.describe(gray)
   
    #print(hist)
	# extract the label from the image path, then update the
	# label and data lists
	labels.append(imagePath.split("/")[-2])
	data.append(hist)
	
	lbp_image=local_binary_pattern(gray,8,2,method='uniform') 
	histogram=scipy1.stats.itemfreq(lbp_image)
	print(histogram)
	


	
	

# train a Linear SVM on the data
svmmodel = LinearSVC(C=100.0, random_state=42)
svmmodel.fit(data, labels)

modelsvm2=svm.SVC(kernel='rbf')
modelsvm2.fit(data, labels)	
	
		
#KNN
# train a Linear SVM on the data
#model = LinearSVC(C=100.0, random_state=42)
from sklearn import neighbors
knnmodel = neighbors.KNeighborsClassifier()
skmodel=knnmodel


skmodel.fit(data, labels)

#KNN
# train a Linear SVM on the data
#model = LinearSVC(C=100.0, random_state=42)
from sklearn.linear_model import LogisticRegression

xmodel = LogisticRegression()


xmodel.fit(data, labels)

# loop over the testing images
for imagePath in paths.list_images('images/testing'):
	# load the image, convert it to grayscale, describe it,
	# and classify it
	image = cv2.imread(imagePath)
	gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
	hist = desc.describe(gray)
	prediction = xmodel.predict(hist)[0]
	print("LR",xmodel.score(data[3:], labels[3:]))
	
	prediction = svmmodel.predict(hist)[0]
	print("SVM",svmmodel.score(data[3:], labels[3:]))
	
	prediction = modelsvm2.predict(hist)[0]
	print("SVM2 ",modelsvm2.score(data[3:], labels[3:]))
	
	
	
	prediction = skmodel.predict(hist)[0]
	print("kNN ",skmodel.score(data[3:], labels[3:]))
	print("mmmm  ",prediction)
 
	print("------------")
	# Learn about API authentication here: https://plot.ly/python/getting-started
	# Find your api_key here: https://plot.ly/settings/api
	# settings for LBP
	import os

	from pyimagesearch.PlotLocalBinaryPatterns import kiko 
	kiko.kenn(os.path.split(imagePath)[-1])
	
	#print(model.predict_proba([[3]]))
	# display the image and the prediction
	#cv2.putText(image, prediction, (10, 30), cv2.FONT_HERSHEY_SIMPLEX,
	#	1.0, (0, 0, 255), 3)
	#cv2.imshow("Image", image)
	#cv2.waitKey(0)
	



import pandas
from sklearn import model_selection
from sklearn.linear_model import LogisticRegression
url = "https://archive.ics.uci.edu/ml/machine-learning-databases/pima-indians-diabetes/pima-indians-diabetes.data"
names = ['preg', 'plas', 'pres', 'skin', 'test', 'mass', 'pedi', 'age', 'class']
dataframe = pandas.read_csv(url, names=names)
array = dataframe.values
X = array[:,0:8]
Y = array[:,8]
seed = 7
kfold = model_selection.KFold(n_splits=10, random_state=seed)
model = LogisticRegression()
scoring = 'accuracy'
results = model_selection.cross_val_score(model, X, Y, cv=kfold, scoring=scoring)
print("Accuracy: %.3f (%.3f)") % (results.mean(), results.std())



# Cross Validation Classification ROC AUC
import pandas
from sklearn import model_selection
from sklearn.linear_model import LogisticRegression
url = "https://archive.ics.uci.edu/ml/machine-learning-databases/pima-indians-diabetes/pima-indians-diabetes.data"
names = ['preg', 'plas', 'pres', 'skin', 'test', 'mass', 'pedi', 'age', 'class']
dataframe = pandas.read_csv(url, names=names)
array = dataframe.values
X = array[:,0:8]
Y = array[:,8]
seed = 7
kfold = model_selection.KFold(n_splits=10, random_state=seed)
model = LogisticRegression()
scoring = 'roc_auc'
results = model_selection.cross_val_score(model, X, Y, cv=kfold, scoring=scoring)
print("AUC: %.3f (%.3f)") % (results.mean(), results.std())






'''
# Spot Check Algorithms
# Split-out validation dataset
# Load dataset
url = "https://archive.ics.uci.edu/ml/machine-learning-databases/iris/iris.data"
names = ['sepal-length', 'sepal-width', 'petal-length', 'petal-width', 'class']
dataset = pandas.read_csv(url, names=names)
array = dataset.values
X = array[:,0:4]
Y = array[:,4]
validation_size = 0.20
seed = 7
X_train, X_validation, Y_train, Y_validation = model_selection.train_test_split(X, Y, test_size=validation_size, random_state=seed)
models = []
models.append(('LR', LogisticRegression()))
models.append(('LDA', LinearDiscriminantAnalysis()))
models.append(('KNN', KNeighborsClassifier()))
models.append(('CART', DecisionTreeClassifier()))
models.append(('NB', GaussianNB()))
models.append(('SVM', SVC()))
# evaluate each model in turn
results = []
names = []
for name, model in models:
	kfold = model_selection.KFold(n_splits=10, random_state=seed)
	cv_results = model_selection.cross_val_score(model, X_train, Y_train, cv=kfold, scoring=scoring)
	results.append(cv_results)
	names.append(name)
	msg = "%s: %f (%f)" % (name, cv_results.mean(), cv_results.std())
	print(msg)
	'''
