# -*- coding: UTF-8 -*-
"""
hello_urlvar: Using URL Variables
"""
from skimage.feature import local_binary_pattern
import matplotlib
from skimage import data as data9
from flask import Flask
app = Flask(__name__)
def describeLocalBinaryPatterns(counter3,id1,databasename,classifier,facealgorithm):
    # USAGE
# python recognize.py --training images/training --testing images/testing

# import the necessary packages
    from pyimagesearch.localbinarypatterns import LocalBinaryPatterns
    from pyimagesearch import  PlotLocalBinaryPatterns
    from skimage.feature import local_binary_pattern
    import matplotlib
    from skimage import data as data9
    # Load libraries
    from scipy.stats import itemfreq
    from sklearn.preprocessing import normalize
    import pandas
    from pandas.tools.plotting import scatter_matrix
    import matplotlib.pyplot as plt
    from sklearn import model_selection
    from sklearn.metrics import classification_report
    from sklearn.metrics import confusion_matrix
    from sklearn.metrics import accuracy_score
    from sklearn.linear_model import LogisticRegression
    from sklearn.tree import DecisionTreeClassifier
    from sklearn.neighbors import KNeighborsClassifier
    from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
    from sklearn.naive_bayes import GaussianNB
    from sklearn.svm import SVC
    import matplotlib.pyplot as plt
    import numpy as np
    import scipy.ndimage as nd
    import skimage.feature as ft
    import pandas
    from pandas.tools.plotting import scatter_matrix
    import matplotlib.pyplot as plt
    from sklearn import model_selection
    from sklearn.metrics import classification_report
    from sklearn.metrics import confusion_matrix
    from sklearn.metrics import accuracy_score
    from sklearn.linear_model import LogisticRegression
    from sklearn.tree import DecisionTreeClassifier
    from sklearn.neighbors import KNeighborsClassifier
    from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
    from sklearn.naive_bayes import GaussianNB
    from sklearn.svm import SVC
    
    from sklearn.svm import LinearSVC
    from imutils import paths
    import argparse
    import cv2
    from  sklearn  import svm
    from timeit import default_timer as timer
    import scipy  as scipy1
    import numpy as np
    import matplotlib.pylab as plt
    import warnings
    warnings.filterwarnings("ignore")
    from sklearn import datasets
    import numpy as np
    import matplotlib.pyplot as plt
    from matplotlib import style
    # construct the argument parse and parse the arguments
    ap = argparse.ArgumentParser()
    #ap.add_argument("-t", "--training", required=True,
        #help="path to the training images")
    #ap.add_argument("-e", "--testing", required=True, 
        #help="path to the tesitng images")
    args = vars(ap.parse_args())
    
    # initialize the local binary patterns descriptor along with
    # the data and label lists
    desc = LocalBinaryPatterns(24, 8)
    data = []
    labels = []
    import numpy as np
    import matplotlib.pyplot as plt
    
    from astropy.visualization import (MinMaxInterval, SqrtStretch,
                                       ImageNormalize)
    count=0
    # loop over the training images
    print "aa  ",counter3,"kennn",id1
    counter35=int(counter3)
    for imagePath in paths.list_images('images/training/cohn-kanade-images'):
        # load the image, convert it to grayscale, and describe it
        count=count+1
        print "aa  ",counter3,"kennn",count
        if(count>counter35):
            break
        image = cv2.imread(imagePath)
        # Create an ImageNormalize object
        #norm = ImageNormalize(image, interval=MinMaxInterval(),
        #                  stretch=SqrtStretch())
        #im = ax.imshow(image, origin='lower', norm=norm)
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        hist = desc.describe(gray)
        
        # Calculate the histogram
        #x = itemfreq(hist2.ravel())
        # Normalize the histogram
        #hist9= x[:, 1]/sum(x[:, 1])
        # Display the query image
        #print(hist)
        # extract the label from the image path, then update the
        # label and data lists
        labels.append(imagePath.split("/")[-2])
        data.append(hist)
        print(count,": count: ",imagePath,"ddddddddd")
        lbp_image=local_binary_pattern(gray,8,2,method='uniform') 
        histogram=scipy1.stats.itemfreq(lbp_image)
        #print(histogram)
        
        count=count+1
    
    
        
        
    accuracy1=0
    # train a Linear SVM on the data
    svmmodel = LinearSVC(C=100.0, random_state=42)
    svmmodel.fit(data, labels)
    modelsvm2=svm.SVC(kernel='rbf')
    modelsvm2.fit(data, labels)    
    from sklearn.ensemble import RandomForestClassifier
    forestmodel  = RandomForestClassifier(n_estimators=10)
    forestmodel.fit(data,labels)# = clf.fit(X, Y)
    from sklearn.model_selection import cross_val_score
    from sklearn.datasets import load_iris
    from sklearn import metrics
    
    from sklearn.ensemble import AdaBoostClassifier
    adaboostmodel = AdaBoostClassifier(n_estimators=100)
    adaboostmodel.fit(data, labels)
    #KNN
    # train a Linear SVM on the data
    #model = LinearSVC(C=100.0, random_state=42)
    from sklearn import neighbors
    knnmodel = neighbors.KNeighborsClassifier(n_neighbors=3,algorithm='kd_tree')
    knnmodel.fit(data, labels)
    y_pred = knnmodel.predict(data)
    print("kk11 KNN",metrics.accuracy_score(labels, y_pred))
    if(classifier=="knn"):
        accuracy1=int(metrics.accuracy_score(labels,y_pred)*100)
    # STEP 1: split X and y into training and testing sets
    from sklearn.cross_validation import train_test_split
    
    
    from sklearn.svm import SVC
    svmmodel= LinearSVC(C=100.0, random_state=42)
    svmmodel.fit(data, labels)
    svm_pred = svmmodel.predict(data)
    print("kk11 linear svm",metrics.accuracy_score(labels,svm_pred))
    if(classifier=="SVM"):
        accuracy1=int(metrics.accuracy_score(labels,svm_pred)*100)
    
    #Cross Validation Test Scenario
    X_train, X_test, y_train, y_test = train_test_split(data, labels, test_size=0.4, random_state=4)
    knn = SVC(kernel='rbf', probability=True)
    knn.fit(X_train, y_train)
    y_pred = knn.predict(X_test)
    print("mmken4n rbf svm is here ",metrics.accuracy_score(y_test, y_pred))
    
    from sklearn.model_selection import cross_val_score
    from sklearn.datasets import make_blobs
    from sklearn.ensemble import RandomForestClassifier
    from sklearn.ensemble import ExtraTreesClassifier
    from sklearn.tree import DecisionTreeClassifier
    from sklearn.svm import SVC
    rfmodel=  RandomForestClassifier(random_state=1)
    rfmodel.fit(data, labels)
    rf_pred = rfmodel.predict(data)
    print("kk11 rf",metrics.accuracy_score(labels,rf_pred))
    
    etcmodel=  RandomForestClassifier(random_state=1)
    etcmodel.fit(data, labels)
    etcmodel_pred = etcmodel.predict(data)
    print("kk11 etcmodel",metrics.accuracy_score(labels,etcmodel_pred))
    if(classifier=="random forest"):
        accuracy1=int(metrics.accuracy_score(labels,rf_pred)*100)
    
        
    from sklearn import neighbors
    knnmodel = neighbors.KNeighborsClassifier(n_neighbors=3,algorithm='kd_tree')
    knnmodel.fit(data, labels)
    y_pred = knnmodel.predict(data)
    print("kk11 KNN",metrics.accuracy_score(labels, y_pred))
    # STEP 1: split X and y into training and testing sets
    
    
    from sklearn.ensemble import VotingClassifier
    # Training classifiers
    X=data
    y=labels
    clf1 = DecisionTreeClassifier(max_depth=4)
    clf2 = KNeighborsClassifier(n_neighbors=7)
    clf3 = SVC(kernel='rbf', probability=True)
    eclf = VotingClassifier(estimators=[('dt', clf1), ('knn', clf2), ('svc', clf3)], voting='hard', weights=[2,5,2])
    clf1 = clf1.fit(X,y)
    clf2 = clf2.fit(X,y)
    clf3 = clf3.fit(X,y)
    eclf = eclf.fit(X,y)
    eclf_pred = eclf.predict(data)
    print("kk11 eclf",metrics.accuracy_score(labels,eclf_pred))
    if(classifier=="SVM"):
        accuracy1=int(metrics.accuracy_score(labels,eclf_pred)*100);
    #id1=101
    #!/usr/bin/python
    import MySQLdb
    # Open database connection
    db = MySQLdb.connect("localhost","chengeke","password","yourdatabasename" )
    # prepare a cursor object using cursor() method
    cursor = db.cursor()
    print  "aw",id1
    # Prepare SQL query to INSERT a record into the database.
    sql = """update  yourdatabasename.Experiment set accuracyclassifier=(%s) where id=101"""
    print sql
    try:# Execute the SQL command
        cursor.execute("UPDATE yourdatabasename.Experiment SET accuracyclassifier = %s WHERE id = %s",(accuracy1, id1))
        db.commit()
    except:
            # Rollback in case there is any error
            db.rollback()
            #disconnect from server
            db.close()
    print "aaaaaaaaaaaaaaaaaaaaaa",accuracy1
   # return metrics.accuracy_score(labels,eclf_pred)
    
def describeLocalDirectionalPattern(counter3,id1,databasename,classifier,facealgorithm):
    # USAGE
    # python recognize.py --training images/training --testing images/testing
    
    # import the necessary packages
    accuracy1=0
    from pyimagesearch.localdirectionalpatterns3 import LocalDirectionalPatterns
    from skimage.feature import local_binary_pattern
    import matplotlib
    from skimage import data as data9
    # Load libraries
    from scipy.stats import itemfreq
    from sklearn.preprocessing import normalize
    import pandas
    from pandas.tools.plotting import scatter_matrix
    import matplotlib.pyplot as plt
    from sklearn import model_selection
    from sklearn.metrics import classification_report
    from sklearn.metrics import confusion_matrix
    from sklearn.metrics import accuracy_score
    from sklearn.linear_model import LogisticRegression
    from sklearn.tree import DecisionTreeClassifier
    from sklearn.neighbors import KNeighborsClassifier
    from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
    from sklearn.naive_bayes import GaussianNB
    from sklearn.svm import SVC
    import matplotlib.pyplot as plt
    import numpy as np
    import scipy.ndimage as nd
    import skimage.feature as ft
    import pandas
    from pandas.tools.plotting import scatter_matrix
    import matplotlib.pyplot as plt
    from sklearn import model_selection                                                                                                                                                                                                                                                                                                        
    from sklearn.metrics import confusion_matrix
    from sklearn.metrics import accuracy_score
    from sklearn.linear_model import LogisticRegression
    from sklearn.tree import DecisionTreeClassifier
    from sklearn.neighbors import KNeighborsClassifier
    from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
    from sklearn.naive_bayes import GaussianNB
    from sklearn.svm import SVC
    
    from sklearn.svm import LinearSVC
    from imutils import paths
    import argparse
    import cv2
    from  sklearn  import svm
    from timeit import default_timer as timer
    import scipy  as scipy1
    import numpy as np
    import matplotlib.pylab as plt
    import warnings
    from datetime import date
    warnings.filterwarnings("ignore")
    from sklearn import datasets
    import numpy as np
    import matplotlib.pyplot as plt
    from matplotlib import style
    # construct the argument parse and parse the arguments
    ap = argparse.ArgumentParser()
    #ap.add_argument("-t", "--training", required=True,
        #help="path to the training images")
    #ap.add_argument("-e", "--testing", required=True, 
        #help="path to the tesitng images")
    args = vars(ap.parse_args())
    
    # initialize the local binary patterns descriptor along with
    # the data and label lists
    desc = LocalDirectionalPatterns(24, 8)
    data = []
    labels = []
    import numpy as np
    import matplotlib.pyplot as plt
    
    from astropy.visualization import (MinMaxInterval, SqrtStretch,
                                       ImageNormalize)
    count=0
    counter4=int(counter3)
    # loop over the training images
    for imagePath in paths.list_images('images/training/cohn-kanade-images'):
        # load the image, convert it to grayscale, and describe it
        count=count+1
        if(count>counter4):
            break
        image = cv2.imread(imagePath)
        # Create an ImageNormalize object
        #norm = ImageNormalize(image, interval=MinMaxInterval(),
        #                  stretch=SqrtStretch())
        #im = ax.imshow(image, origin='lower', norm=norm)
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        hist = desc.kirsch_filter(gray)
        
        # Calculate the histogram
        #x = itemfreq(hist2.ravel())
        # Normalize the histogram
        #hist9= x[:, 1]/sum(x[:, 1])
        # Display the query image
        #print(hist)
        # extract the label from the image path, then update the
        # label and data lists
        labels.append(imagePath.split("/")[-2])
        data.append(hist)
        print(count,": count: ",imagePath,"ddddddddd")
        lbp_image=local_binary_pattern(gray,8,2,method='uniform') 
        histogram=scipy1.stats.itemfreq(lbp_image)
        #print(histogram)
        
        count=count+1
    
    
        
        
    
    # train a Linear SVM on the data
    svmmodel = LinearSVC(C=100.0, random_state=42)
    svmmodel.fit(data, labels)
    modelsvm2=svm.SVC(kernel='rbf')
    modelsvm2.fit(data, labels)    
    from sklearn.ensemble import RandomForestClassifier
    forestmodel  = RandomForestClassifier(n_estimators=10)
    forestmodel.fit(data,labels)# = clf.fit(X, Y)
    from sklearn.model_selection import cross_val_score
    from sklearn.datasets import load_iris
    from sklearn import metrics
    
    from sklearn.ensemble import AdaBoostClassifier
    adaboostmodel = AdaBoostClassifier(n_estimators=100)
    adaboostmodel.fit(data, labels)
    #KNN
    # train a Linear SVM on the data
    #model = LinearSVC(C=100.0, random_state=42)
    from sklearn import neighbors
    knnmodel = neighbors.KNeighborsClassifier(n_neighbors=3,algorithm='kd_tree')
    knnmodel.fit(data, labels)
    y_pred = knnmodel.predict(data)
    print("kk11 KNN",metrics.accuracy_score(labels, y_pred))
    # STEP 1: split X and y into training and testing sets
    from sklearn.cross_validation import train_test_split
    
    
    from sklearn.svm import SVC
    svmmodel= LinearSVC(C=100.0, random_state=42)
    svmmodel.fit(data, labels)
    svm_pred = svmmodel.predict(data)
    print("kk11 linear svm",metrics.accuracy_score(labels,svm_pred))
    if(classifier=="SVM"):
        accuracy1=int(metrics.accuracy_score(labels,svm_pred)*100)
    
    #Cross Validation Test Scenario
    X_train, X_test, y_train, y_test = train_test_split(data, labels, test_size=0.4, random_state=4)
    knn = SVC(kernel='rbf', probability=True)
    knn.fit(X_train, y_train)
    y_pred = knn.predict(X_test)
    print("mmken4n rbf svm is here ",metrics.accuracy_score(y_test, y_pred))
    
    from sklearn.model_selection import cross_val_score
    from sklearn.datasets import make_blobs
    from sklearn.ensemble import RandomForestClassifier
    from sklearn.ensemble import ExtraTreesClassifier
    from sklearn.tree import DecisionTreeClassifier
    from sklearn.svm import SVC
    rfmodel=  RandomForestClassifier(random_state=1)
    rfmodel.fit(data, labels)
    rf_pred = rfmodel.predict(data)
    print("kk11 rf",metrics.accuracy_score(labels,rf_pred))
    if(classifier=="random forest"):
        accuracy1=metrics.accuracy_score(labels,rf_pred)
    etcmodel=  RandomForestClassifier(random_state=1)
    etcmodel.fit(data, labels)
    etcmodel_pred = etcmodel.predict(data)
    print("kk11 etcmodel",metrics.accuracy_score(labels,etcmodel_pred))
    
    
        
    from sklearn import neighbors
    knnmodel = neighbors.KNeighborsClassifier(n_neighbors=3,algorithm='kd_tree')
    knnmodel.fit(data, labels)
    y_pred = knnmodel.predict(data)
    print("kk11 KNN",metrics.accuracy_score(labels, y_pred))
    # STEP 1: split X and y into training and testing sets
    if(classifier=="knn"):
        accuracy1=int(metrics.accuracy_score(labels,y_pred)*100)
    
    from sklearn.ensemble import VotingClassifier
    # Training classifiers
    X=data
    y=labels
    clf1 = DecisionTreeClassifier(max_depth=4)
    clf2 = KNeighborsClassifier(n_neighbors=7)
    clf3 = SVC(kernel='rbf', probability=True)
    eclf = VotingClassifier(estimators=[('dt', clf1), ('knn', clf2), ('svc', clf3)], voting='hard', weights=[2,5,2])
    clf1 = clf1.fit(X,y)
    clf2 = clf2.fit(X,y)
    clf3 = clf3.fit(X,y)
    eclf = eclf.fit(X,y)
    eclf_pred = eclf.predict(data)
    print("kk11 eclf",metrics.accuracy_score(labels,eclf_pred))
    if(classifier=="voting classifier"):
        accuracy1=int(metrics.accuracy_score(labels,eclf_pred)*100);
    #accuracy1=int(metrics.accuracy_score(labels,eclf_pred)*100);
    #id1=101
    #!/usr/bin/python
    import MySQLdb
    # Open database connection
    db = MySQLdb.connect("localhost","chengeke","password","yourdatabasename" )
    # prepare a cursor object using cursor() method
    cursor = db.cursor()
    print  "aw",id1
    # Prepare SQL query to INSERT a record into the database.
    sql = """update  yourdatabasename.Experiment set accuracyclassifier=(%s) where id=101"""
    print sql
    try:# Execute the SQL command
        cursor.execute("UPDATE yourdatabasename.Experiment SET accuracyclassifier = %s WHERE id = %s",(accuracy1, id1))
        db.commit()
    except:
            # Rollback in case there is any error
            db.rollback()
            #disconnect from server
            db.close()
    print "aaaaaaaaaaaaaaaaaaaaaa",accuracy1
   # return metrics.accuracy_score(labels,eclf_pred)
        





@app.route('/hello/',methods=['GET'])
def hello():
    print "s"
    counter3 = request.args.get('counter3')
    id1 = request.args.get('id1')
    facealgorithm = request.args.get('facealgorithm')
    classifier = request.args.get('classifier')
    databasename= request.args.get('databasename')
    print "aa  ",counter3,"kennn",facealgorithm
    print(id1)
    if(facealgorithm=="Local Binary Pattern"):
        describeLocalBinaryPatterns(counter3,id1,databasename,classifier,facealgorithm)
    if(facealgorithm=="Local Directional Pattern"):
        describeLocalDirectionalPattern(counter3,id1,databasename,classifier,facealgorithm)
    return 'Hello, world78!'

from flask import Flask,  request

import json
import os.path
@app.route('/hello/<username>')  # URL with a variable
def hello_username(username):    # The function shall take the URL variable as parameter
    return 'Hello, {}'.format(username)

@app.route('/hello/<int:userid>')  # Variable with type filter. Accept only int
def hello_userid(userid):          # The function shall take the URL variable as parameter
    return 'Hello, your ID is: {:d}'.format(userid)

if __name__ == '__main__':
    app.run(debug=True)  # Enable reloader and debugger
    
    
# import the necessary packages
from skimage import feature
import numpy as np
