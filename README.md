**Facial Expression Recognition **

**The following details the work done for the PHD Research on Facial Expression and is divided into the followign sections 

1. Deep Learning Python Code using KEras
2. Deep Learning Code using TensorFlow
3. Optmization of the python code with L2 Regulaization, Dropout, Augmentation 
4. Local Binary PAtterns Algorithms using Python implementation 
5. Implementation of the bottle framework for REST in python to expose the FER as a service framework
6. Various algorithms in python for classification- Voting Classifier, Support Vector Machines, K Nearest Neigbor etc
7. Proposed Algorithm patterns and implementation in Python
8. API implementation in IBM API Connect
9. Implementation in IBM BPM for BPMN and BPEL models**

